require('dotenv').config();

const { WebClient } = require('@slack/web-api');
const { createMessageAdapter } = require('@slack/interactive-messages');
const dayjs = require('dayjs');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const testRoute = require('./src/testRoute');
const config = require('./src/config');

const app = express();
const slackInteractions = createMessageAdapter(config.slakcSignIn);
const webClientInstance = new WebClient(config.slackOAuthToken);

app.use(cors());

slackInteractions.action({ type: 'static_select' }, (payload, respond) => {
  try {
    console.log('payload', payload);

    webClientInstance.views
      .open({
        trigger_id: payload.trigger_id,
        view: {
          type: 'modal',
          title: {
            type: 'plain_text',
            text: 'modal',
          },
          blocks: [
            {
              type: 'section',
              block_id: 'section-identifier',
              text: {
                type: 'plain_text',
                text: 'section',
              },
              accessory: {
                type: 'button',
                text: {
                  type: 'plain_text',
                  text: 'Just a button',
                },
                action_id: 'button-identifier',
              },
            },
          ],
        },
      })
      .catch((err) => console.log('view error:', err));
  } catch (error) {
    console.log('error', error);
  }
});

app.use('/slack', slackInteractions.requestListener());

app.use(bodyParser.json());

app.get('/', (req, res) => res.send('it works'));

app.use('/test', testRoute);

const PORT = process.env.PORT || 5000;

(async () => {
  try {
    await webClientInstance.chat.postMessage({
      channel: 'channel_id_here',
      as_user: 'USER_ID_HERE',
      text: 'testing web api',
      blocks: [
        {
          type: 'section',
          fields: [{ type: 'plain_text', text: 'field1' }],
          accessory: {
            type: 'static_select',
            placeholder: {
              type: 'plain_text',
              text: 'static select',
            },
            options: [
              {
                text: {
                  type: 'plain_text',
                  text: 'option1',
                },
                value: 'option1',
              },
            ],
          },
        },
      ],
    });
  } catch (error) {
    console.log('error', error);
  }
})();

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
