const express = require('express');

const router = express.Router();

const payload = {
  type: 'modal',
  title: {
    type: 'plain_text',
    text: 'testing',
  },
  submit: {
    type: 'plain_text',
    text: 'submit',
  },
  close: {
    type: 'plain_text',
    text: 'cancel',
  },
  blocks: [
    {
      type: 'input',
      block_id: 'test',
      element: {
        type: 'plain_text_input',
        action_id: 'test',
      },
      label: {
        type: 'plain_text',
        text: 'say hello',
      },
    },
  ],
};

router.post('/', (req, res) => {
  console.log('res', res);
  console.log('req', req);
  res.json(payload);
});

module.exports = router;
