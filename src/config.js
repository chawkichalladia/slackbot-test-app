function configValue(key, fallback = 1) {
  return process.env[key] || fallback;
}

module.exports = {
  slakcSignIn: configValue('SLACK_SIGNIN'),
  slackOAuthToken: configValue('SLACK_OAUTH_TOKEN'),
};
